const descripcion = {
  demand: true,
  alias: "d",
  desc: "Descripción de la tarea por hacer",
};

const completado = {
  alias: "c",
  desc: "Marca como completado o pendiente la tarea",
};

const argv = require("yargs")
  .command("crear", "Crea un elemento por hacer", {
    descripcion,
  })
  .command("listar", "Lista todas las tareas por hacer", {
    completado,
  })
  .command("actualizar", "Actualiza el estado completado de una tarea", {
    descripcion,
    completado: {
      ...completado,
      default: true,
    },
  })
  .command("borrar", "Borra una tarea de la lista por hacer", {
    descripcion,
  })
  .help().argv;

module.exports = {
  argv,
};
